package android.com.gbtoktaproto

import android.com.gbtoktaproto.GbtApplication.Companion.client
import android.content.Intent
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.okta.oidc.AuthorizationStatus
import com.okta.oidc.OIDCConfig
import com.okta.oidc.Okta
import com.okta.oidc.ResultCallback
import com.okta.oidc.clients.web.WebAuthClient
import com.okta.oidc.storage.SharedPreferenceStorage
import com.okta.oidc.util.AuthorizationException
import kotlinx.android.synthetic.main.activity_home.*




class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val sessionClient = client.sessionClient

        val callback = object : ResultCallback<AuthorizationStatus, AuthorizationException> {
            override fun onSuccess(@NonNull status: AuthorizationStatus) {
                startActivity(Intent(this@HomeActivity, AuthorizedActivity::class.java))
                finish()
            }

            override fun onCancel() {}

            override fun onError(@Nullable msg: String?, error: AuthorizationException?) {}
        }

        client.registerCallback(callback, this)

        btnLogin.setOnClickListener {
            client.signIn(this, null)
        }

        if (sessionClient.isAuthenticated) {
            startActivity(Intent(this, AuthorizedActivity::class.java))
            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //must pass the results back to the WebAuthClient.
        client.handleActivityResult(requestCode, resultCode, data)

    }
}
