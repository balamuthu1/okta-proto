package android.com.gbtoktaproto

import android.com.gbtoktaproto.GbtApplication.Companion.client
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.okta.oidc.RequestCallback
import com.okta.oidc.util.AuthorizationException
import kotlinx.android.synthetic.main.activity_authorized.*


class AuthorizedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorized)


        val sessionClient = client.sessionClient

        txtInfo.text =  "token id: \n${sessionClient.tokens.idToken}\n\n\n" +
                "access token: \n${sessionClient.tokens.accessToken}\n\n\n" +
                "refresh token: \n${sessionClient.tokens.refreshToken}\n\n\n"


        btnLogout.setOnClickListener {
            client.signOut(this, object : RequestCallback<Int, AuthorizationException>{
                override fun onSuccess(result: Int) {
                    startActivity(Intent(this@AuthorizedActivity, HomeActivity::class.java))
                    finish()
                }

                override fun onError(error: String?, exception: AuthorizationException?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })
            client.sessionClient.clear()
        }
    }

    override fun onStop() {
        finish()
        super.onStop()
    }

}
