package android.com.gbtoktaproto

import android.app.Application
import android.content.Context
import com.okta.oidc.OIDCConfig
import com.okta.oidc.Okta
import com.okta.oidc.clients.web.WebAuthClient
import com.okta.oidc.storage.SharedPreferenceStorage

class GbtApplication: Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        val config = OIDCConfig.Builder()
            .clientId("0oa1nwsthq8xP8tWs357")
            .redirectUri("com.okta.dev-672975:/login")
            .endSessionRedirectUri("{com.okta.dev-672975:/logout")
            .scopes("openid", "profile", "offline_access")
            .discoveryUri("https://dev-672975.okta.com/")
            .create()

        client = Okta.WebAuthBuilder()
            .withConfig(config)
            .withContext(this)
            .withStorage(SharedPreferenceStorage(this))
            .create()

    }

    companion object {
        private var instance: GbtApplication? = null
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
        lateinit var client: WebAuthClient
    }
}